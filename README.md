
## IPFS Server
- Android IPFS Server with CLI support and WebUI


### IPFS
The InterPlanetary File System "is a peer-to-peer (p2p) filesharing system that aims to fundamentally change the way information is distributed across & beyond the globe. IPFS consists of several innovations in communication protocols and distributed systems that have been combined to produce a file system like no other."
<br>
"It attempts to address the deficiencies of the client-server model and HTTP web through a novel p2p file sharing system."
<br>
Source: https://hackernoon.com/a-beginners-guide-to-ipfs-20673fedd3f
<br>
Official website of IPFS: https://ipfs.io/


### Features 
- IPFS Server with CLI support and WebUI


### Dependencies 
- go-ipfs (IPFS node implementation)
<br>Source : https://github.com/ipfs/go-ipfs
<br>Note : The executables of the IPFS node implementation are located in threads/src/main/assets
<br>Download from https://github.com/ipfs/go-ipfs/releases
<br>Linux versions (64 bit) amd64 and arm64 (copy "ipfs" executables and rename it to amd64 and arm64)
<br>Linux version  (32 bit) x86 (copy "ipfs" executable from go-ipfs_v0.4.18_linux-386 and rename to x86)
- threads-ipfs (CLI interface)
<br>Source : https://gitlab.com/remmer.wilts/threads-ipfs
- MinSdkVersion 24 (Android 7.0)




